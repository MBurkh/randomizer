﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Randomizer
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public Dictionary<int, string> Options { get; private set; }
		public Dictionary<int, int> OptionsWithLevels { get; private set; }
		public int CurrentRound { get; private set; }
		public int MinLevel { get; private set; }
		public int MaxLevel { get; private set; }
		public int MaxLevelPlayed { get; private set; }
		public int BonusRoundsPlayed { get; private set; }
		public bool WithPulsation { get; private set; }
		public bool WithIncreasingLevel { get; private set; }

		public MainWindow()
		{
			InitializeComponent();
			InitializeLevelCombos();
			Options = new Dictionary<int, string>();
			OptionsWithLevels = new Dictionary<int, int>();
			CurrentRound = 1;
			MinLevel = 0;
			MaxLevel = 0;
			MaxLevelPlayed = 0;
			WithPulsation = false;
			WithIncreasingLevel = false;
		}

		private void InitializeLevelCombos()
		{
			List<int> levels = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8 };
			ComboMin.ItemsSource = levels;
			ComboMax.ItemsSource = levels;
		}

		private void InitOptionLists(int minLevel, int maxLevel, bool withPulsation)
		{
			for(int i = minLevel; i <= maxLevel; i++)
			{
				Options.Add(i, String.Format("Stufe {0}", i));
				OptionsWithLevels.Add(i, i);
			}

			if (withPulsation)
			{
				Options.Add(9, "Pulsierend Muster 1");
				Options.Add(10, "Pulsierend Muster 2");
				Options.Add(11, "Pulsierend Muster 3");
				Options.Add(12, "Pulsierend Muster 4");
				Options.Add(13, "Pulsierend Muster 5");
				Options.Add(14, "Pulsierend Muster 6");
				Options.Add(15, "Pulsierend Muster 7");
				Options.Add(16, "Pulsierend Muster 8");

				OptionsWithLevels.Add(9, 0);
				OptionsWithLevels.Add(10, 0);
				OptionsWithLevels.Add(11, 0);
				OptionsWithLevels.Add(12, 0);
				OptionsWithLevels.Add(13, 0);
				OptionsWithLevels.Add(14, 0);
				OptionsWithLevels.Add(15, 0);
				OptionsWithLevels.Add(16, 0);
			}
		}

		private void ButtonStart_Click(object sender, RoutedEventArgs e)
		{
			MinLevel = (int)ComboMin.SelectedValue;
			MaxLevel = (int)ComboMax.SelectedValue;
			WithPulsation = CheckPulsating.IsChecked ?? false;
			WithIncreasingLevel = CheckIncreasing.IsChecked ?? false;

			InitOptionLists(MinLevel, MaxLevel, WithPulsation);

			Grid_Setup.Visibility = Visibility.Hidden;
			Grid_Result.Visibility = Visibility.Visible;

			GenerateRandom();
		}

		private void ButtonNext_Click(object sender, RoutedEventArgs e)
		{
			GenerateRandom();
		}

		private void ButtonReset_Click(object sender, RoutedEventArgs e)
		{
			Options = new Dictionary<int, string>();
			OptionsWithLevels = new Dictionary<int, int>();
			CurrentRound = 1;
			MinLevel = 0;
			MaxLevel = 0;
			MaxLevelPlayed = 0;
			BonusRoundsPlayed = 0;
			WithPulsation = false;
			WithIncreasingLevel = false;

			ComboMax.SelectedIndex = -1;
			ComboMin.SelectedIndex = -1;
			CheckIncreasing.IsChecked = false;
			CheckPulsating.IsChecked = false;
			ButtonStart.IsEnabled = false;

			Grid_Result.Visibility = Visibility.Hidden;
			Grid_Setup.Visibility = Visibility.Visible;
		}

		private void GenerateRandom()
		{
			int rndMax = 0;
			int newOption = 0;
			int newLevel = 0;

			if (CurrentRound > MaxLevel && MaxLevelPlayed == MaxLevel)
			{
				LabelRound.Content = String.Empty;
				LabelDuration.Content = String.Empty;
				LabelLevel.Content = "Spielende!";
				return;
			}

			if (WithPulsation)
				rndMax = 16;
			else
				rndMax = MaxLevel;

			Random random = new Random();
			while (true)
			{
				newOption = random.Next(MinLevel, rndMax + 1);

				if (!Options.ContainsKey(newOption))
					continue;

				OptionsWithLevels.TryGetValue(newOption, out newLevel);

				if (newLevel > MaxLevel)
					continue;

				if (BonusRoundsPlayed == 3 && newOption > 8)
					continue;

				if (WithIncreasingLevel && (newLevel < CurrentRound) && (newLevel != 0))
					continue;

				break;
			}

			if (MaxLevelPlayed < newLevel)
				MaxLevelPlayed = newLevel;

			Options.TryGetValue(newOption, out string newLevelText);
			LabelLevel.Content = newLevelText;

			int newLevelDuration = random.Next(1, 7);
			if (newLevelDuration == 1)
				LabelDuration.Content = "1 Minute";
			else
				LabelDuration.Content = String.Format("{0} Minuten", newLevelDuration);

			if (newOption < 9)
			{
				LabelRound.Content = String.Format("Runde {0}", CurrentRound);
				CurrentRound++;
			}
			else
			{
				LabelRound.Content = "Bonusrunde!";
				BonusRoundsPlayed++;
			}
		}

		private void SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			if (ComboMax.SelectedIndex == -1 || ComboMin.SelectedIndex == -1)
			{
				ButtonStart.IsEnabled = false;
				return;
			}

			if ((int)ComboMax.SelectedValue < (int)ComboMin.SelectedValue)
			{
				ButtonStart.IsEnabled = false;
				return;
			}

			ButtonStart.IsEnabled = true;
		}
	}
}
